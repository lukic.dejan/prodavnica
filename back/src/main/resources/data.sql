INSERT INTO `user` (id, username, password, role)
              VALUES (1,'miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','ADMIN');
INSERT INTO `user` (id, username, password, role)
              VALUES (2,'tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','KORISNIK');
INSERT INTO `user` (id, username, password, role)
              VALUES (3,'petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','KORISNIK');




INSERT INTO kategorija (id, name) VALUES (1, 'Monitori');
INSERT INTO kategorija (id, name) VALUES (2, 'Tastature');
INSERT INTO kategorija (id, name) VALUES (3, 'Slusalice');


INSERT INTO proizvod (id, name, cena, stanje, kategorija_id) 
	VALUES (1, 'Monitor LG19', 24499, 7, 1);
INSERT INTO proizvod (id, name, cena, stanje, kategorija_id) 
	VALUES (2, 'Lenovo tastatura', 1599, 4, 2);
INSERT INTO proizvod (id, name, cena, stanje, kategorija_id) 
	VALUES (3, 'Gaming slusalice', 7699, 5, 3);
INSERT INTO proizvod (id, name, cena, stanje, kategorija_id) 
	VALUES (4, 'Monitor LG24', 20199, 7, 1);
INSERT INTO proizvod (id, name, cena, stanje, kategorija_id) 
	VALUES (5, 'Genius tastatura', 1299, 4, 2);
INSERT INTO proizvod (id, name, cena, stanje, kategorija_id) 
	VALUES (6, 'XWave slusalice', 5599, 5, 3);	

              
INSERT INTO porudzbina (id, kolicina, proizvod_id) VALUES (1, 5, 1);
INSERT INTO porudzbina (id, kolicina, proizvod_id) VALUES (2, 3, 2);
INSERT INTO porudzbina (id, kolicina, proizvod_id) VALUES (3, 6, 3);
INSERT INTO porudzbina (id, kolicina, proizvod_id) VALUES (4, 11, 4);
INSERT INTO porudzbina (id, kolicina, proizvod_id) VALUES (5, 2, 5);
INSERT INTO porudzbina (id, kolicina, proizvod_id) VALUES (6, 7, 6);	