package prodavnica.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import prodavnica.model.Proizvod;


@Repository
public interface ProizvodRepository extends JpaRepository<Proizvod, Long>{

	@Query("SELECT p FROM Proizvod p WHERE" +
			"(:minCena = NULL OR :maxCena = NULL OR p.cena BETWEEN :minCena AND :maxCena) AND " +
			"(:kategorijaId = NULL OR p.kategorija.id = :kategorijaId)")
	Page<Proizvod> search(@Param("minCena") Double minCena, @Param("maxCena") Double maxCena, @Param("kategorijaId") Long kategorijaId, Pageable pageable);

//	Proizvod findOneById(Long id);

//	@Query("SELECT COALESCE(SUM(t.points),0) FROM Task t WHERE t.sprint.id = :sprintId")
//	Long sumPoints(Long sprintId);

}
