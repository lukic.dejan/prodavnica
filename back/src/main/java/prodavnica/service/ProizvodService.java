package prodavnica.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import prodavnica.model.Proizvod;
import prodavnica.web.dto.ProizvodDto;

public interface ProizvodService {
	
	Page<Proizvod> search(Double minCena, Double maxCena, Long kategorijaId, int pageNo);
	Page<Proizvod> all(int page);
	Optional<Proizvod> one(Long id);
	Proizvod save(ProizvodDto proizvod);
	Proizvod delete(Long id);
	
}
