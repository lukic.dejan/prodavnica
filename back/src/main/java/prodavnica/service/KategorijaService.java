package prodavnica.service;

import java.util.List;
import java.util.Optional;

import prodavnica.model.Kategorija;

public interface KategorijaService {

	List<Kategorija> all();
	Optional<Kategorija> one(Long id);
	Kategorija save(Kategorija kategorija);

}
