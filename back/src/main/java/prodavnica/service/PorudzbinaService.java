package prodavnica.service;

import java.util.List;
import java.util.Optional;

import prodavnica.model.Porudzbina;

public interface PorudzbinaService {

	List<Porudzbina> all();
	Optional<Porudzbina> one(Long id);
	Porudzbina save(Porudzbina porudzbina);

}
