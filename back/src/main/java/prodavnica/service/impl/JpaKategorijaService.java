package prodavnica.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import prodavnica.model.Kategorija;
import prodavnica.repository.KategorijaRepository;
import prodavnica.service.KategorijaService;

@Service
public class JpaKategorijaService implements KategorijaService {
	
	@Autowired
	private KategorijaRepository kategorijaRepository;

	@Override
	public List<Kategorija> all() {
		return kategorijaRepository.findAll();
	}

	@Override
	public Optional<Kategorija> one(Long id) {
		return kategorijaRepository.findById(id);
	}

	@Override
	public Kategorija save(Kategorija kategorija) {
		return kategorijaRepository.save(kategorija);
	}
	
	
}
