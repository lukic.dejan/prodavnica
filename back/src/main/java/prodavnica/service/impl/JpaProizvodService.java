package prodavnica.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import prodavnica.model.Proizvod;
import prodavnica.model.Porudzbina;
import prodavnica.model.Kategorija;
import prodavnica.repository.KategorijaRepository;
import prodavnica.repository.PorudzbinaRepository;
import prodavnica.repository.ProizvodRepository;
import prodavnica.service.ProizvodService;
import prodavnica.support.ProizvodDtoToProizvod;
import prodavnica.web.dto.ProizvodDto;


@Service
@Transactional
public class JpaProizvodService implements ProizvodService {
	
	@Autowired
	private ProizvodDtoToProizvod toEntity;
	
	@Autowired
	private ProizvodRepository proizvodRepository;
	
	@Autowired
	private KategorijaRepository kategorijaRepository;
	
	@Autowired
	private PorudzbinaRepository porudzbinaRepository;
	
	@Override
	public Page<Proizvod> all(int pageNo) {
		return proizvodRepository.findAll(PageRequest.of(pageNo, 20));
	}
	
	@Override
	public Page<Proizvod> search(Double minCena, Double maxCena, Long kategorijaId, int pageNo){
		return proizvodRepository.search(minCena,maxCena, kategorijaId, PageRequest.of(pageNo, 20));
	}
	
	@Override
	public Optional<Proizvod> one(Long id) {
		return proizvodRepository.findById(id);
	}

	@Override
	public Proizvod save(ProizvodDto proizvodDto) {
				
		Proizvod proizvod= toEntity.convert(proizvodDto);

		if(proizvodDto.getId() != null) {
			Optional<Proizvod> oldProzivodOptional = one(proizvodDto.getId());
			if(oldProzivodOptional.isPresent()) {
				Proizvod oldProizvod= oldProzivodOptional.get();
				Kategorija oldKategorija = oldProizvod.getKategorija();
				kategorijaRepository.save(oldKategorija);
			}
		}
			
		Kategorija kategorija = proizvod.getKategorija();
		Proizvod savedProizvod = proizvodRepository.save(proizvod);
		kategorijaRepository.save(kategorija);
		return savedProizvod;
	}


	@Override
	@Transactional
	public Proizvod delete(Long id) {
		Optional<Proizvod> proizvodOptional = proizvodRepository.findById(id);
		if(proizvodOptional.isPresent()) {
			Proizvod proizvod = proizvodOptional.get();
			
			Kategorija kategorija = proizvod.getKategorija();
			List<Porudzbina> porudzbina = proizvod.getPorudzbine();
			
			porudzbinaRepository.saveAll(porudzbina);
			kategorijaRepository.save(kategorija);
			proizvodRepository.deleteById(id);
			return proizvod;
		}
		return null;
	}
}
