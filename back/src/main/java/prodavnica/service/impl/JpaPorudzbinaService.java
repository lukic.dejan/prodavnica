package prodavnica.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import prodavnica.model.Porudzbina;
import prodavnica.repository.PorudzbinaRepository;
import prodavnica.service.PorudzbinaService;

@Service
public class JpaPorudzbinaService implements PorudzbinaService {
	
	@Autowired
	private PorudzbinaRepository porudzbinaRepository;

	@Override
	public List<Porudzbina> all() {
		return porudzbinaRepository.findAll();
	}

	@Override
	public Optional<Porudzbina> one(Long id) {
		return porudzbinaRepository.findById(id);
	}
	
	@Override
	public Porudzbina save(Porudzbina porudzbina) {
		
		return porudzbinaRepository.save(porudzbina);
	}

}
