package prodavnica.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import prodavnica.model.Proizvod;
import prodavnica.web.dto.ProizvodDto;


@Component
public class ProizvodToProizvodDTO implements Converter<Proizvod, ProizvodDto>{

	@Override
	public ProizvodDto convert(Proizvod source) {
		
		ProizvodDto retValue = new ProizvodDto();
		retValue.setId(source.getId());
		retValue.setName(source.getName());
		retValue.setCena(source.getCena());
		retValue.setStanje(source.getStanje());
		
		retValue.setKategorijaId(source.getKategorija().getId());
		retValue.setKategorijaName(source.getKategorija().getName());

		return retValue;
	}

	public List<ProizvodDto> convert(List<Proizvod> source){
		List<ProizvodDto> ret = new ArrayList<>();
		
		for(Proizvod p : source){
			ret.add(convert(p));
		}
		
		return ret;
	}

}
