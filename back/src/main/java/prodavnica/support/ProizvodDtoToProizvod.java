package prodavnica.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import prodavnica.model.Proizvod;
import prodavnica.model.Kategorija;
import prodavnica.service.KategorijaService;
import prodavnica.web.dto.ProizvodDto;

@Component
public class ProizvodDtoToProizvod implements Converter<ProizvodDto, Proizvod>{
	
	@Autowired
	private KategorijaService kategorijaService;
	
	@Override
	public Proizvod convert(ProizvodDto source) {

		Kategorija kategorija = null;
		if(source.getKategorijaId() != null) {
			kategorija = kategorijaService.one(source.getKategorijaId()).get();
		}
				
		if(kategorija!=null) {
			
			Proizvod proizvod = new Proizvod();
			
			if(proizvod != null) {
				proizvod.setId(source.getId());
				proizvod.setName(source.getName());
				proizvod.setCena(source.getCena());
				proizvod.setStanje(source.getStanje());
				
				proizvod.setKategorija(kategorija);
				
			}
			
			return proizvod;
			
		}else {
			throw new IllegalStateException("Trying to attach to non-existant entities");
		}
		
	}
}
