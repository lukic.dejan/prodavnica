package prodavnica.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import prodavnica.model.Porudzbina;
import prodavnica.web.dto.PorudzbinaDto;

@Component
public class PorudzbinaToPorudzbinaDto implements Converter<Porudzbina, PorudzbinaDto>{

	@Override
	public PorudzbinaDto convert(Porudzbina source) {
		PorudzbinaDto dto = new PorudzbinaDto();
		dto.setId(source.getId());
		dto.setKolicina(source.getKolicina());
		
		dto.setProizvodId(source.getProizvod().getId());;
		dto.setProizvodName(source.getProizvod().getName());
		
		return dto;
	}
	
	public List<PorudzbinaDto> convert(List<Porudzbina> source){
		List<PorudzbinaDto> retVal = new ArrayList<>();
		
		for(Porudzbina s : source) {
			retVal.add(convert(s));
		}
		
		return retVal;
	}

}
