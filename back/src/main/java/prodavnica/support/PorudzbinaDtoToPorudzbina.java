package prodavnica.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import prodavnica.model.Porudzbina;
import prodavnica.model.Proizvod;
import prodavnica.service.ProizvodService;
import prodavnica.web.dto.PorudzbinaDto;

@Component
public class PorudzbinaDtoToPorudzbina implements Converter<PorudzbinaDto, Porudzbina>{
	
	@Autowired
	private ProizvodService proizvodService;

	@Override
	public Porudzbina convert(PorudzbinaDto source) {
		
		Proizvod proizvod = null;
		if (source.getProizvodId() != null) {
			proizvod = proizvodService.one(source.getProizvodId()).get();
			
		}
		if (proizvod != null) {
			
			Porudzbina porudzbina = new Porudzbina();
			
			if (porudzbina !=null) {
				porudzbina.setId(source.getId());
				porudzbina.setKolicina(source.getKolicina());
				porudzbina.setProizvod(proizvod);
								
			}
			return porudzbina;	
		}else {
			throw new IllegalStateException("Trying to attach to non-existant entities");
		}
		
	}

}
