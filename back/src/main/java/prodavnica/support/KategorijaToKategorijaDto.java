package prodavnica.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import prodavnica.model.Kategorija;
import prodavnica.web.dto.KategorijaDto;

@Component
public class KategorijaToKategorijaDto implements Converter<Kategorija, KategorijaDto>{

	@Override
	public KategorijaDto convert(Kategorija source) {
		KategorijaDto dto = new KategorijaDto();
		dto.setId(source.getId());
		dto.setName(source.getName());
		
		return dto;
	}
	
	public List<KategorijaDto> convert(List<Kategorija> source){
		List<KategorijaDto> retVal = new ArrayList<>();
		
		for(Kategorija k : source) {
			KategorijaDto dto = convert(k);
			retVal.add(dto);
		}
		
		return retVal;
	}

}
