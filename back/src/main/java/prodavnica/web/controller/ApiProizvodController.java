package prodavnica.web.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import prodavnica.model.Proizvod;
import prodavnica.service.ProizvodService;
import prodavnica.support.ProizvodToProizvodDTO;
import prodavnica.web.dto.ProizvodDto;

@RestController
@RequestMapping(value = "/api/proizvodi")
public class ApiProizvodController {
	
	@Autowired
	private ProizvodService proizvodService;

	@Autowired
	private ProizvodToProizvodDTO toDTO;
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity<List<ProizvodDto>> get(
			@RequestParam(value = "minCena", required = false) Double minCena,
			@RequestParam(value = "maxCena", required = false) Double maxCena,
			@RequestParam(value = "kategorijaId", required = false) Long kategorijaId,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {

		Page<Proizvod> page = null;

		if (minCena != null || maxCena !=null || kategorijaId != null) {
			page = proizvodService.search(minCena, maxCena, kategorijaId, pageNo);
		} else {
			page = proizvodService.all(pageNo);
		}

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));
		
		return new ResponseEntity<>(toDTO.convert(page.getContent()), headers, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	ResponseEntity<ProizvodDto> getOne(@PathVariable Long id) {
		Optional<Proizvod> proizvod= proizvodService.one(id);
		if (!proizvod.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDTO.convert(proizvod.get()), HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	ResponseEntity<ProizvodDto> delete(@PathVariable Long id) {
		Proizvod deleted = proizvodService.delete(id);

		if (deleted == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDTO.convert(deleted), HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<ProizvodDto> add(@Validated @RequestBody ProizvodDto newDto) {

		Proizvod saved = proizvodService.save(newDto);

		return new ResponseEntity<>(toDTO.convert(saved), HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
	public ResponseEntity<ProizvodDto> edit(@Validated @RequestBody ProizvodDto prozivodDto, @PathVariable Long id) {

		if (!id.equals(prozivodDto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Proizvod persisted = proizvodService.save(prozivodDto);

		return new ResponseEntity<>(toDTO.convert(persisted), HttpStatus.OK);
	}
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
