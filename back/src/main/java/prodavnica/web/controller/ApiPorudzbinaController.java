package prodavnica.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import prodavnica.model.Porudzbina;
import prodavnica.service.PorudzbinaService;
import prodavnica.support.PorudzbinaToPorudzbinaDto;
import prodavnica.web.dto.PorudzbinaDto;

@RestController
@RequestMapping("api/porudzbine")
public class ApiPorudzbinaController {

	@Autowired
	private PorudzbinaService porudzbinaService;
	
	@Autowired
	private PorudzbinaToPorudzbinaDto toDto;
	
	@PreAuthorize("hasRole('KORISNIK')")
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<PorudzbinaDto> add(@Validated @RequestBody Porudzbina porudzbina) {

		Porudzbina saved = porudzbinaService.save(porudzbina);

		return new ResponseEntity<>(toDto.convert(saved), HttpStatus.CREATED);
	}
	
}
