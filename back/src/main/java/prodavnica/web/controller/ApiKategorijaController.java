package prodavnica.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import prodavnica.model.Kategorija;
import prodavnica.service.KategorijaService;
import prodavnica.support.KategorijaToKategorijaDto;
import prodavnica.web.dto.KategorijaDto;

@RestController
@RequestMapping("api/kategorije")
public class ApiKategorijaController {

	@Autowired
	private KategorijaService kategorijaService;
	
	@Autowired
	private KategorijaToKategorijaDto toDto;
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping
	public ResponseEntity<List<KategorijaDto>> getAll(
			@RequestParam(required = false) String name){
		
		List<Kategorija> kategorije = kategorijaService.all();
		return new ResponseEntity<>(toDto.convert(kategorije), HttpStatus.OK);
	}
	
	
}
