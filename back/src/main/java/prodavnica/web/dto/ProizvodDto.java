package prodavnica.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

public class ProizvodDto {
	
	private Long id;
	@NotBlank
	@Length(max = 15)
	private String name;
	
	@Positive
	private Double cena;
	@Positive
	private Integer stanje;	
	
	private Long kategorijaId;
	private String kategorijaName;	
	
	public ProizvodDto() {
		
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Double getCena() {
		return cena;
	}


	public void setCena(Double cena) {
		this.cena = cena;
	}


	public Integer getStanje() {
		return stanje;
	}


	public void setStanje(Integer stanje) {
		this.stanje = stanje;
	}


	public Long getKategorijaId() {
		return kategorijaId;
	}


	public void setKategorijaId(Long kategorijaId) {
		this.kategorijaId = kategorijaId;
	}

	public String getKategorijaName() {
		return kategorijaName;
	}


	public void setKategorijaName(String kategorijaName) {
		this.kategorijaName = kategorijaName;
	}




	

	
	

}
