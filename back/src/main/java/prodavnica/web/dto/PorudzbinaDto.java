package prodavnica.web.dto;

public class PorudzbinaDto {
	
	private Long id;
	private Integer kolicina;
	
	private Long proizvodId;
	private String proizvodName;	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getProizvodId() {
		return proizvodId;
	}
	public void setProizvodId(Long proizvodId) {
		this.proizvodId = proizvodId;
	}
	public String getProizvodName() {
		return proizvodName;
	}
	public void setProizvodName(String proizvodName) {
		this.proizvodName = proizvodName;	
	}
	public Integer getKolicina() {
		return kolicina;
	}
	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}
	
	
	
	

	
}
