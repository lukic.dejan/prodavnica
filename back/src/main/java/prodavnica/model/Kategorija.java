package prodavnica.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Kategorija {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column
	String name;
	
	@OneToMany(mappedBy = "kategorija", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	List<Proizvod> proizvodi = new ArrayList<Proizvod>();

	public Kategorija() {
		super();
	}
	
	public Kategorija(Long id, String name, List<Proizvod> proizvodi) {
		super();
		this.id = id;
		this.name = name;
		this.proizvodi = proizvodi;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Proizvod> getProizvodi() {
		return proizvodi;
	}

	public void setProizvodi(List<Proizvod> proizvodi) {
		this.proizvodi = proizvodi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "Kategorija [Id=" + id + ", name=" + name + ", proizvodi=" + proizvodi + "]";
	}

}
