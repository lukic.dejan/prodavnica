package prodavnica.model;

import javax.persistence.*;

@Entity
public class Porudzbina {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column( nullable = false)
	Integer kolicina;	
	
	@ManyToOne
	Proizvod proizvod;
	
	public Porudzbina() {
		super();
	}

	public Porudzbina(Long id, Integer kolicina, Proizvod proizvod) {
		super();
		this.id = id;
		this.kolicina = kolicina;
		this.proizvod = proizvod;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public Proizvod getProizvod() {
		return proizvod;
	}

	public void setProizvod(Proizvod proizvod) {
		this.proizvod = proizvod;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Porudzbina other = (Porudzbina) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Porudzbina [id=" + id + ", kolicina=" + kolicina + ", proizvod=" + proizvod + "]";
	}
	

}
