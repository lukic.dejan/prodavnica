package prodavnica.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Proizvod {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column( nullable = false, unique = true)
	String name;
	
	@Column (nullable = false)
	Double cena;
	
	@Column (nullable = false)
	Integer stanje;
	
	@OneToMany(mappedBy = "proizvod", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	List<Porudzbina> porudzbine = new ArrayList<Porudzbina>();
	
	@ManyToOne
	Kategorija kategorija;
	
	
	public Proizvod() {
		super();
	}

	public Proizvod(Long id, String name, Double cena, Integer stanje, List<Porudzbina> porudzbine,
			Kategorija kategorija) {
		super();
		this.id = id;
		this.name = name;
		this.cena = cena;
		this.stanje = stanje;
		this.porudzbine = porudzbine;
		this.kategorija = kategorija;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Integer getStanje() {
		return stanje;
	}

	public void setStanje(Integer stanje) {
		this.stanje = stanje;
	}

	public List<Porudzbina> getPorudzbine() {
		return porudzbine;
	}

	public void setPorudzbine(List<Porudzbina> porudzbine) {
		this.porudzbine = porudzbine;
	}

	public Kategorija getKategorija() {
		return kategorija;
	}

	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proizvod other = (Proizvod) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Proizvod [id=" + id + ", name=" + name + ", cena=" + cena + ", stanje=" + stanje + ", porudzbina="
				+ porudzbine + ", kategorija=" + kategorija + "]";
	}	

//	public void removeProizvod (Long id) {
//		for(Proizvod p : this.proizvod) {
//			if (t.getId()==id){
//				this.tasks.remove(t);
//				this.setPoints(Integer.parseInt(this.getPoints())-t.getPoints()+"");
//				return;
//			}
//		}
//	}
//
//	public void addProizvod(Proizvod proizvod) {
//		this.proizvodi.add(proizvod);
//		this.setPoints(Integer.parseInt(this.getPoints())+task.getPoints()+"");		
//	}
	
}
