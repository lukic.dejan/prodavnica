import React from "react";
import { Button, Form } from "react-bootstrap";
import { withNavigation, withParams } from "../../routeconf";
import ProdavnicaAxios from "../../apis/ProdavnicaAxios";

class EditProizvod extends React.Component {
  constructor(props) {
    super(props);

    let proizvod = {
      name: "",
      cena: 0,
      stanje: 0,
      kategorijaId: -1
    };

    this.state = {
      proizvod: proizvod,
      kategorije: [],
      porudzbine: []
      
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    await this.getKategorije();
    await this.getPorudzbine();
    await this.getProizvodi();
  }

  async getProizvodi() {
    
    try {
      let result = await ProdavnicaAxios.get("/proizvodi/" + this.props.params.id);
      if (result && result.status === 200) {
        this.setState({
          proizvod: result.data
        });
      }
    } catch (error) {
      alert("Nije uspelo dobavljanje.");
    }
  }

  async getKategorije() {
    try {
      let result = await ProdavnicaAxios.get("/kategorije");
      if (result && result.status === 200) {
        this.setState({
          kategorije: result.data,
        });
      }
    } catch (error) {
      alert("Nije uspelo dobavljanje.");
    }
  }

  async getPorudzbine() {
    try {
      let result = await ProdavnicaAxios.get("/porudzbine");
      if (result && result.status === 200) {
        this.setState({
          porudzbine: result.data,
        });
      }
    } catch (error) {
      alert("Nije uspelo dobavljanje.");
    }
  }

  async doEdit() {
    try {
      await ProdavnicaAxios.put("/proizvodi/" + this.props.params.id, this.state.proizvod);
      this.props.navigate("/proizvodi");
    } catch (error) {
      alert("Nije uspelo čuvanje.");
    }
  }

  valueInputChange(event) {
    let control = event.target;

    let name = control.name;
    let value = control.value;

    let proizvod = this.state.proizvod;
    proizvod[name] = value;

    this.setState({ proizvod: proizvod });
  }

  render() {
    return (
      <div>
        <h1>Proizvodi</h1>

        <Form>
          <Form.Group>
            <Form.Label>Naziv</Form.Label>
            <Form.Control
              onChange={(event) => this.valueInputChange(event)}
              name="name"
              value={this.state.proizvod.name}
              as="input"
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Cena</Form.Label>
            <Form.Control
              onChange={(event) => this.valueInputChange(event)}
              name="cena"
              value={this.state.proizvod.cena}
              as="input"
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Stanje</Form.Label>
            <Form.Control
              onChange={(event) => this.valueInputChange(event)}
              name="stanje"
              as="input"
              type="number"
              min = "0"
              step = "1"
              value={this.state.proizvod.stanje}
            ></Form.Control>
          </Form.Group>
          
          <Form.Group>
            <Form.Label>Kategorija</Form.Label>
            <Form.Control
              onChange={(event) => this.valueInputChange(event)}
              name="kategorijaId"
              value={this.state.proizvod.kategorijaId}
              as="select"
            >
              <option value={-1}></option>
              {this.state.kategorije.map((kategorija) => {
                return (
                  <option value={kategorija.id} key={kategorija.id}>
                    {kategorija.name}
                  </option>
                );
              })}
            </Form.Control>
          </Form.Group>
          <Button variant="primary" onClick={() => this.doEdit()}>
            Edit
          </Button>
        </Form>
      </div>
    );
  }
}

export default withNavigation(withParams(EditProizvod));
