import React from "react";
import { Table, Button, Form, ButtonGroup, Collapse } from "react-bootstrap";
import { withNavigation } from "../../routeconf";
import ProdavnicaAxios from "../../apis/ProdavnicaAxios";

class Proizvodi extends React.Component {
  constructor(props) {
    super(props);

    //potrebno je zbog create-a
    let proizvod = {
      name: "",
      cena: "",
      stanje: "",
      porudzbinaKolicina: "",
      kategorijaId: -1,
    };

    this.state = {
      proizvod: proizvod,
      proizvodi: [],
      kategorije: [],
      showSearch: false,
      search: { minCena: "", maxCena: "", kategorijaId: -1 },
      pageNo: 0,
      totalPages: 1,
      
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    
    await this.getProizvodi();
    await this.getKategorije();
  }

  async getProizvodi(page) {
    let config = { params: {
      pageNo: page
    } };

    //Sledeca 3 if-a su tu zbog search-a
    if (this.state.search.minCena !== "") {
      config.params.minCena = this.state.search.minCena;
    }

    if (this.state.search.maxCena !== "") {
      config.params.maxCena = this.state.search.maxCena;
    }

    if (this.state.search.kategorijaId !== -1) {
      config.params.kategorijaId = this.state.search.kategorijaId;
    }

    try {
      let result = await ProdavnicaAxios.get("/proizvodi", config);
      if (result && result.status === 200) {

        this.setState({
          pageNo: page,
          proizvodi: result.data,
          totalPages: result.headers["total-pages"]
          

        });
      }
    } catch (error) {
      alert("Nije uspelo dobavljanje.");
    }
  }

  async getKategorije() {
    try {
      let result = await ProdavnicaAxios.get("/kategorije");
      if (result && result.status === 200) {
        this.setState({
          kategorije: result.data,
        });
      }
    } catch (error) {
      alert("Nije uspelo dobavljanje.");
    }
  }

  goToEdit(proizvodId) {
    this.props.navigate("/proizvodi/edit/" + proizvodId);
  }

  async doAdd() {
    try {
      await ProdavnicaAxios.post("/proizvodi", this.state.proizvod);

      //bitno je da bi "resetovali" polja za kreiranje nakon kreiranja
      let proizvod = {
        name: "",
        cena: "",
        stanje: "",
        kategorijaId: -1
      };
      this.setState({ proizvod: proizvod });

      console.log(proizvod);

      this.getProizvodi();
    } catch (error) {
      alert("Nije uspelo dodavanje.");
    }
  }

  async doDelete(proizvodId) {
    try {
      await ProdavnicaAxios.delete("/proizvodi/" + proizvodId);
      var nextPage
      if(this.state.pageNo===this.state.totalPages-1 && this.state.proizvodi.length===1){
        nextPage = this.state.pageNo-1
      }else{
        nextPage = this.state.pageNo
      }
      await this.getProizvodi(nextPage);
    } catch (error) {
      alert("Nije uspelo brisanje.");
    }
  }

  addValueInputChange(event) {
    let control = event.target;

    let name = control.name;
    let value = control.value;

    let proizvod = this.state.proizvod;
    proizvod[name] = value;

    this.setState({ proizvod: proizvod });
  }

  searchValueInputChange(event) {
    let control = event.target;

    let name = control.name;
    let value = control.value;

    let search = this.state.search;
    search[name] = value;

    this.setState({ search: search });
  }

  doSearch() {
    this.getProizvodi(0);
  }

  canCreateTask(){
    const proizvod = this.state.proizvod
    return proizvod.name!=="" && 
    
       proizvod.kategorijaId !== -1
  }


  

  render() {
    return (
      <div>
        <h1>Proizvodi</h1>
        {/*Deo za ADD*/}
        {window.localStorage['role']==="ROLE_ADMIN"?
        <Form>
          <Form.Group>
            <Form.Label>Naziv</Form.Label>
            <Form.Control
              onChange={(event) => this.addValueInputChange(event)}
              name="name"
              value={this.state.proizvod.name}
              as="input"
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Cena</Form.Label>
            <Form.Control
              onChange={(event) => this.addValueInputChange(event)}
              name="cena"
              value={this.state.proizvod.cena}
              as="input"
              type="number"
              min = "0"
              step = "1"
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Stanje</Form.Label>
            <Form.Control
              onChange={(event) => this.addValueInputChange(event)}
              name="stanje"
              value={this.state.proizvod.stanje}
              as="input"
              type="number"
              min = "0"
              step = "1"
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Kategorija</Form.Label>
            <Form.Control
              onChange={(event) => this.addValueInputChange(event)}
              name="kategorijaId"
              value={this.state.proizvod.kategorijaId}
              as="select"
            >
              <option value={-1}></option>
              {this.state.kategorije.map((kategorija) => {
                return (
                  <option value={kategorija.id} key={kategorija.id}>
                    {kategorija.name}
                  </option>
                );
              })}
            </Form.Control>
          </Form.Group>
          <Button disabled = {!this.canCreateTask()} variant="primary" onClick={() => this.doAdd()}>
            Add
          </Button>
        </Form>:null}

        {/*Deo za Search*/}
        <Form.Group style={{marginTop:35}}>
          <Form.Check type="checkbox" label="Show search form" onClick={(event) => this.setState({showSearch: event.target.checked})}/>
        </Form.Group>
        <Collapse in={this.state.showSearch}>
        <Form style={{marginTop:10}}>
          <Form.Group>
            <Form.Label>Min Cena</Form.Label>
            <Form.Control
              value={this.state.search.minCena}
              name="minCena"
              as="input"
              type="number"
              min = "0"
              step = "1"
              onChange={(e) => this.searchValueInputChange(e)}
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Max Cena</Form.Label>
            <Form.Control
              value={this.state.search.maxCena}
              name="maxCena"
              as="input"
              type="number"
              min = "0"
              step = "1"
              onChange={(e) => this.searchValueInputChange(e)}
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Kategorija</Form.Label>
            <Form.Control
              onChange={(event) => this.searchValueInputChange(event)}
              name="kategorijaId"
              value={this.state.search.kategorijaId}
              as="select"
            >
              <option value={-1}></option>
              {this.state.kategorije.map((kategorija) => {
                return (
                  <option value={kategorija.id} key={kategorija.id}>
                    {kategorija.name}
                  </option>
                );
              })}
            </Form.Control>
          </Form.Group>
          <Button onClick={() => this.doSearch()}>Search</Button>
        </Form>
        </Collapse>

        {/*Deo za prikaz Task-a*/}
        <ButtonGroup style={{ marginTop: 25, float:"right"}}>
          <Button 
            style={{ margin: 3, width: 90}}
            disabled={this.state.pageNo===0} onClick={()=>this.getProizvodi(this.state.pageNo-1)}>
            Prethodna
          </Button>
          <Button
            style={{ margin: 3, width: 90}}
            disabled={this.state.pageNo===this.state.totalPages-1} onClick={()=>this.getProizvodi(this.state.pageNo+1)}>
            Sledeća
          </Button>
        </ButtonGroup>

        <Table bordered striped style={{ marginTop: 5 }}>
          <thead className="thead-dark">
            <tr>
              <th>Naziv</th>
              <th>Cena</th>
              <th>Stanje</th>
              <th>Kolicina</th>
              <th colSpan={2}>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.state.proizvodi.map((proizvod) => {
              return (
                <tr key={proizvod.id}>
                  <td>{proizvod.name}</td>
                  <td>{proizvod.cena}</td>
                  <td>{proizvod.stanje}</td>
                  <td>{proizvod.olicina}</td>
                  <td>
                    {window.localStorage['role']==="ROLE_ADMIN"?
                    [<Button
                      variant="warning"
                      onClick={() => this.goToEdit(proizvod.id)}
                      style={{ marginLeft: 5 }}
                    >
                      Edit
                    </Button>,

                    <Button
                      variant="danger"
                      onClick={() => this.doDelete(proizvod.id)}
                      style={{ marginLeft: 5 }}
                    >
                      Delete
                    </Button>]:null}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default withNavigation(Proizvodi);
