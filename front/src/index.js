import React from "react";
import ReactDOM from "react-dom";
import {
  Route,
  Link,
  HashRouter as Router,
  Routes
} from "react-router-dom";
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import { Container, Navbar, Nav, Button } from "react-bootstrap";
import Login from "./components/Login/Login";
import { logout } from "./services/auth";
import Proizvodi from "./components/proizvodi/Proizvodi";
import EditProizvod from "./components/proizvodi/EditProizvod";

class App extends React.Component {
  render() {
      return (
        <div>
          <Router>
            <Navbar bg="dark" variant="dark" expand>
              <Navbar.Brand as={Link} to="/">
                JWD
              </Navbar.Brand>
              <Nav className="mr-auto">
                <Nav.Link as={Link} to="/proizvodi">
                  Proizvodi
                </Nav.Link>
              </Nav>

              {window.localStorage['jwt'] ? 
                  <Button onClick = {()=>logout()}>Log out</Button> :
                  <Nav.Link as={Link} to="/login">Log in</Nav.Link>
              }
            </Navbar>

            <Container style={{marginTop:25}}>
              <Routes>
                <Route path="/" element={<Home/>} />
                <Route path="/proizvodi" element={<Proizvodi/>} />
                <Route path="/proizvodi/edit/:id" element={<EditProizvod/>} />
                <Route path="/login" element={<Login/>}/>
                <Route path="*" element={<NotFound/>} />
              </Routes>
            </Container>
          </Router>
        </div>
      );
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
